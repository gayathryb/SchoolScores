package com.nyc.schools.activities;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nyc.schools.R;
import com.nyc.schools.models.School;

import java.util.List;


public class SchoolsAdapter extends RecyclerView.Adapter<SchoolsAdapter.ContentHolder> {
    private List<School> schoolsList;
    private SchoolTapListener listener;

    public class ContentHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public String dbn;

        public ContentHolder(View view) {
            super(view);
            title = view.findViewById(R.id.school_name);
        }
    }


    public SchoolsAdapter(List<School> schoolsList, SchoolTapListener listener) {
        this.schoolsList = schoolsList;
        this.listener = listener;
    }

    @Override
    public ContentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.school_list_row, parent, false);

        return new ContentHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ContentHolder holder, int position) {
        final School school = schoolsList.get(position);
        holder.dbn = school.dbn;
        holder.title.setText(school.name);
        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onTap(school.dbn);
            }
        });
    }

    @Override
    public int getItemCount() {
        return schoolsList.size();
    }
}
