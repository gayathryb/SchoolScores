package com.nyc.schools.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nyc.schools.R;
import com.nyc.schools.models.Score;
import com.nyc.schools.network.WebServicesManager;
import com.nyc.schools.network.ScoresRequestListener;

public class ScoreDetails extends AppCompatActivity {
    private static final String LOG_TAG = ScoreDetails.class.getSimpleName();
    private ProgressBar loader;
    private TextView title;
    private TextView totalTestTakers;
    private TextView readingScore;
    private TextView mathScore;
    private TextView writingScore;
    private TextView errMsg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_satscore_details);

        loader = findViewById(R.id.loader);

        title = findViewById(R.id.school_name);
        totalTestTakers = findViewById(R.id.total_test_takers_value);
        readingScore = findViewById(R.id.reading_value);
        mathScore = findViewById(R.id.math_value);
        writingScore = findViewById(R.id.writing_value);
        errMsg = findViewById(R.id.error_msg);

        String dbn = getIntent().getStringExtra("com.example.schoolsactivity.dbn");

        WebServicesManager.getInstance(this).fetchSATScores(dbn, new ScoresRequestListener() {
            @Override
            public void onSuccess(Score score) {
                loader.setVisibility(View.GONE);
                if (score != null) {
                    title.setText(score.schoolName);
                    totalTestTakers.setText(score.totalTestTakers);
                    readingScore.setText(score.readingScore);
                    mathScore.setText(score.mathScore);
                    writingScore.setText(score.writingScore);
                } else {
                    Log.d(LOG_TAG, "No details found");
                    errMsg.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onError(String errorMessage) {
                loader.setVisibility(View.GONE);
                Log.d(LOG_TAG, errorMessage);
                errMsg.setVisibility(View.VISIBLE);
            }
        });
    }
}

