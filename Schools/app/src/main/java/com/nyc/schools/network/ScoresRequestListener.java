package com.nyc.schools.network;

import com.nyc.schools.models.Score;

public interface ScoresRequestListener {
    void onSuccess(Score score);
    void onError(String errorMessage);
}
