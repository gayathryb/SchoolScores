package com.nyc.schools.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.nyc.schools.models.Score;

import org.json.JSONArray;
import org.json.JSONObject;

public class WebServicesManager {

    private static Context mContext;
    private static WebServicesManager manager;
    private RequestQueue mRequestQueue;

    private WebServicesManager(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized WebServicesManager getInstance(Context context) {
        if (manager == null) {
            manager = new WebServicesManager(context);
        }
        return manager;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mContext);
        }
        return mRequestQueue;
    }

    public void fetchSchools(final SchoolsRequestListener listener) {
        final String url = "https://data.cityofnewyork.us/resource/97mf-9njv.json";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                listener.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.getMessage());
            }
        });

        getRequestQueue().add(jsonArrayRequest);
    }

    public void fetchSATScores(final String dbn, final ScoresRequestListener listener) {
        final String url = "https://data.cityofnewyork.us/resource/734v-jeq5.json";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Boolean found = false;
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        Score satScoreObj = new Gson().fromJson(obj.toString(), Score.class);
                        if (satScoreObj.dbn.equals(dbn)) {
                            listener.onSuccess(satScoreObj);
                            found = true;
                            break;
                        }
                    } catch (Exception e) {
                        Log.d("Exception", e.getLocalizedMessage());
                    }
                }

                if (!found) {
                    listener.onSuccess(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.getMessage());
            }
        });

        getRequestQueue().add(jsonArrayRequest);
    }
}
