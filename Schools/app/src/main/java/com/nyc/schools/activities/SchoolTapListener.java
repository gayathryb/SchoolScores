package com.nyc.schools.activities;


public interface SchoolTapListener {
    void onTap(String dbn);
}
