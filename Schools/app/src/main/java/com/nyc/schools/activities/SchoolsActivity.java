package com.nyc.schools.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nyc.schools.R;
import com.nyc.schools.models.School;
import com.nyc.schools.network.SchoolsRequestListener;
import com.nyc.schools.network.WebServicesManager;

import org.json.JSONArray;

import java.util.Arrays;
import java.util.List;

public class SchoolsActivity extends AppCompatActivity  {
    private static final String LOG_TAG = SchoolsActivity.class.getSimpleName();
    public static final String SELECTED_SCHOOL_DBN = "com.example.schoolsactivity.dbn";
    private RecyclerView recyclerView;
    private SchoolsAdapter mAdapter;
    private ProgressBar loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schools);

        loader = findViewById(R.id.loader);
        recyclerView = findViewById(R.id.recycler_view);

        WebServicesManager.getInstance(SchoolsActivity.this).fetchSchools(new SchoolsRequestListener() {
            @Override
            public void onSuccess(JSONArray response) {
                loader.setVisibility(View.GONE);
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                List<School> schools = Arrays.asList(gson.fromJson(response.toString(), School[].class));

                mAdapter = new SchoolsAdapter(schools, new SchoolTapListener() {
                    @Override
                    public void onTap(String dbn) {
                        Log.d(LOG_TAG, dbn);

                        Intent intent = new Intent(SchoolsActivity.this, ScoreDetails.class);
                        intent.putExtra(SELECTED_SCHOOL_DBN, dbn);
                        startActivity(intent);
                    }
                });

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.addItemDecoration(new DividerItemDecoration(SchoolsActivity.this, LinearLayoutManager.VERTICAL));

                recyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onError(String errorMessage) {
                loader.setVisibility(View.GONE);
                Toast.makeText(SchoolsActivity.this, R.string.schools_fetch_error, Toast.LENGTH_SHORT).show();
                Log.d(LOG_TAG, errorMessage);
            }
        });
    }

}
