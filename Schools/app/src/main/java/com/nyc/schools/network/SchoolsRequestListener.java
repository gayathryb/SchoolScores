package com.nyc.schools.network;


import org.json.JSONArray;

public interface SchoolsRequestListener {
    void onSuccess(JSONArray response);
    void onError(String errorMessage);
}
