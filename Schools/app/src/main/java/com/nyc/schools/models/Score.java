package com.nyc.schools.models;


import com.google.gson.annotations.SerializedName;

public class Score {
    public String dbn;

    @SerializedName("num_of_sat_test_takers")
    public String totalTestTakers;

    @SerializedName("sat_critical_reading_avg_score")
    public String readingScore;

    @SerializedName("sat_math_avg_score")
    public String mathScore;

    @SerializedName("sat_writing_avg_score")
    public String writingScore;

    @SerializedName("school_name")
    public String schoolName;
}
