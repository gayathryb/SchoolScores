package com.nyc.schools.models;

import com.google.gson.annotations.SerializedName;

public class School {
    public String dbn;

    @SerializedName("school_name")
    public String name;

    public School(String dbn, String name) {
        this.dbn = dbn;
        this.name = name;
    }
}
